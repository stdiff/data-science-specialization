# Getting and Cleaning Data: Course Project

This GitHub repository contains the following files

- `run_analysis.R` : the main R script.
- `tidy_data.txt`  : the tidy data which is required by the course project
- `Codebook.md`    : description of rawdata and the processed data
- `README.md`      : the file you are now reading!

## How to use the script

The rawdata is not contained in this repository. So you need to get it
from the course website.

Put the ZIP file `getdata_projectfiles_UCI HAR Dataset.zip` in the same
directory where `run_analysis.R` is. Then unzip the ZIP file.

Executing the R script at the same directory, you will get the text file
`tidy_data.txt`. If you are working on Linux, then the easiest way is
the following command. 

	$ Rscript run_analysis.R
