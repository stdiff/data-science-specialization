# Experimental design

Each examinee performed six activities (WALKING, WALKING_UPSTAIRS, WALKING_DOWNSTAIRS, SITTING, STANDING, LAYING) wearing a smartphone (Samsung Galaxy S II) on the waist.

# Rawdata

3-axial linear acceleration and 3-axial angular velocity at a constant rate of 50Hz.

# Processed data

The first column (subjectID) is the IDs of examinees and the second column (activity) is the names of activities. The other column is acturally the average of the values of the columns of the variable in the rawdata by subject ID and activity.
