# Assignments of Data Science Specialization 

"[Data Science Specialization](https://www.coursera.org/specializations/jhu-data-science)" is a series of courses of coursera. [My review about it](http://stdiff.net/?pa2016010401) can be found in my blog.

This repository contains materials which I created for the following assignments of the courses.

- ProgrammingAssignment2 (R Programming)
- tidy_data (Getting and Cleaning Data)
- ExData_Plotting1 (Exploratory Data Analysis)
- writeup (Practical Machine Learning)

These are just part of all assignments of the course series, because I did only part of the series.