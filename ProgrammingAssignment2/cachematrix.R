### An object of a matrix for storing the inverse matrix

## x <- makeCacheMatrix() create an object
## $set()                 set the matrix
## $get()                 get the matrix
## $setinverse()          set the inverse
## $getinverse()          get the inverse
makeCacheMatrix <- function(x = matrix()) {
    inv <- NULL
    set <- function(y) {
        x <<- y;
        inv <<- NULL;
    }
    get <- function() x
    setinverse <- function(sol) inv <<- sol
    getinverse <- function() inv
    list(set=set, get=get, setinverse=setinverse, getinverse=getinverse)
}

## set the inverse matrix for an object created by makeCacheMatrix
cacheSolve <- function(x, ...) {
    inv <- x$getinverse()
    if(!is.null(inv)) {
        message("getting cached data")
        return(inv)
    }
    mat <- x$get()
    inv <- solve(mat,...)
    x$setinverse(inv)
    ## Return a matrix that is the inverse of 'x'
    inv
}
